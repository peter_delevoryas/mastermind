#include <iostream>
#include <utility>
#include <stdio.h>
#include <vector>
#include <string.h>
#include <queue>

struct Code;
struct CodeSet;
struct Response;

CodeSet all_codes();
void all_codes_helper(CodeSet &set, Code code, size_t pos);
Response check_guess(Code key, Code guess);

// Represents a single mastermind code
struct Code {
    static constexpr size_t LEN = 3;
    static constexpr size_t COLORS = 2;
    size_t vec[Code::LEN];
    void print() {
        printf("[");
        for (int i = 0; i < Code::LEN - 1; ++i) {
            printf("%lu, ", vec[i]);
        }
        printf("%lu", vec[Code::LEN - 1]);
        printf("]");
    }
};

struct CodeSet {
    // member var
    std::vector<Code> vec;

    void print() {
        printf("[");
        for (size_t i = 0; i < vec.size() - 1; ++i) {
            vec[i].print();
            printf(",\n");
        }
        vec[vec.size() - 1].print();
        printf("]");
    }
};

struct Response {
    uint8_t vec[Code::LEN];
    static constexpr uint8_t COLOR_AND_POS = 2;
    static constexpr uint8_t COLOR_NOT_POS = 1;
    static constexpr uint8_t NONE = 0;
    void print() {
        for (size_t i = 0; i < Code::LEN; ++i) {
            if (vec[i] == COLOR_AND_POS) {
                printf("X ");
            } else if (vec[i] == COLOR_NOT_POS) {
                printf("O ");
            } else {
                printf("N ");
            }
        }
    }
};

CodeSet all_codes() {
    CodeSet codes;
    Code init;
    memset(&init, 0, sizeof(Code));
    all_codes_helper(codes, init, 0);
    return codes;
}

void all_codes_helper(CodeSet &set, Code code, size_t pos) {
    if (pos == Code::LEN) {
        set.vec.push_back(code);
        return;
    }
    for (int i = 0; i < Code::COLORS; ++i) {
        Code copy = code;
        copy.vec[pos] = i;
        all_codes_helper(set, copy, pos+1);
    }
}

/*
A colored or black key peg is placed for each code peg from the guess
which is correct in both color and position. A white key peg indicates
the existence of a correct color code peg placed in the wrong position

if a previous response == current and previous response == Correct pos andcolor, unique = ffalse
*/

Response check_guess(Code key, Code guess) {
    Response r;
    memset(&r, 0, sizeof(Response));
    std::queue<size_t> work;
    for (size_t i = 0; i < Code::LEN; ++i) {
        if (key.vec[i] == guess.vec[i]) {
            r.vec[i] = Response::COLOR_AND_POS;
        } else {
            work.push(i);
        }
    }
    //printf("work: %lu\n", work.size());
    size_t i;
    while (!work.empty()) {
        i = work.front();
        bool correct_color = false;
        bool unique = true;
        for (size_t j = 0; j < Code::LEN; ++j) {
            if (i == j) {
                continue;
            }
            if (guess.vec[j] == guess.vec[i] && r.vec[j] == Response::COLOR_AND_POS) {
                unique = false;
            }
            if (key.vec[j] == guess.vec[i]) {
                correct_color = true;
            }
        }
        if (unique && correct_color) {
            r.vec[i] == Response::COLOR_NOT_POS;
        }
        work.pop();
    }
    return r;
}

void reduce_set(CodeSet cs, Response r) {
    // if color and pos, remove all without color and pos
    // if correct color not pos, 
    // if null remove
}


int main() {
    Code answer = { 0, 1, 2 };
    answer.print();
    printf("\n");

    // initialize all possible codes
    CodeSet possible;
    possible = all_codes();
    //possible.print();
    //printf("\n");
    //printf("%d\n", Response::COLOR_AND_POS);
    possible.print();
    printf("\n\n");
    for (auto guess : possible.vec) {
        auto r = check_guess(answer, guess);
        r.print();
        printf("\n");
    }
}
