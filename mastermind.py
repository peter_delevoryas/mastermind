"""
Peter Delevoryas
CSCI 2824
Challenge Problems 1.1 and 1.2

    Usage: python mastermind.py

prints the solution to 1.1, the guess used in 1.2,
and the final solution to 1.2

"""

color = "BORYPW"
# Function used to print codes
def code_to_str(code):
    s = ""
    for x in code:
        s += color[x] + ' '
    return s

# Function to generate all possible
# code permutations
def gen_ps(cset, code, x):
    if x == 4:
        cset.append(code)
        return
    for i in range(6):
        copy = list(code)
        copy[x] = i
        gen_ps(cset, copy, x + 1)

# Function to return x/o response
# equivalent of checking the secret
# code against a guess and setting
# colored and white pegs
def check(guess, code):
    resp = ""
    gcpy = list(guess)
    ccpy = list(code)
    xs = 0
    os = 0
    # First, count the number of
    # "x" matches, and eliminate
    # them from future matches
    # by storing a placeholder
    for i in range(4):
        if guess[i] == code[i]:
            gcpy[i] = 'a'
            ccpy[i] = 'b'
            xs += 1
    # gcpy now holds all of the
    # elements in the original
    # guess, however guesses that
    # were "x" matches are now
    # replaced with 'a's, and
    # their corresponding ccpy
    # elements are replaced with 'b's
    # if an element is an "o" match,
    # it is still replaced with 'a'
    # to ensure that future "o" matches
    # don't consider it
    for i in range(4):
        if gcpy[i] == 'a':
            continue
        for j in range(4):
            if gcpy[i] == ccpy[j]:
                os += 1
                gcpy[i] = 'a'
                ccpy[j] = 'b'
    resp = xs*'x' + os*'o'
    return resp

# Given a constraint (a guess
# and a response) reduce the
# set of possible secret codes
# Uses check() with each code
# to see if the constraint is
# satisfied: if not, remove
# from set of possible codes
# Returns the reduced set of
# codes.
def reduce(cset, guess, resp):
    reduced_set = []
    for code in cset:
        if check(guess, code) == resp:
            reduced_set.append(code)
    return reduced_set

# Challenge Problem 1.1
# Answer found: YORO
cset = []
gen_ps(cset, [ 0, 0, 0, 0 ], 0)
cset = reduce(cset, [ 5, 1, 0, 2 ], "xo")
cset = reduce(cset, [ 4, 3, 2, 0 ], "xo")
cset = reduce(cset, [ 2, 2, 1, 3 ], "ooo")
cset = reduce(cset, [ 3, 2, 2, 5 ], "xx")
print("Challenge Problem 1.1, secret code: ", code_to_str(cset[0]))

# Challenge Problem 1.2
# Answer found: OOPW
cset = []
gen_ps(cset, [ 0, 0, 0, 0 ], 0)
cset = reduce(cset, [ 1, 3, 4, 2 ], "xx")
cset = reduce(cset, [ 3, 0, 1, 5 ], "xo")
cset = reduce(cset, [ 0, 5, 3, 3 ], "o")

cset = reduce(cset, [ 1, 3, 1, 1 ], "xo")
print("Challenge Problem 1.2, secret code: ", code_to_str(cset[0]))
print("Challenge Problem 1.2, guess  used: ", code_to_str([ 1, 3, 1, 1 ]))

# Find a guess that, when used to reduce cset
# once more, finds a solution set of length 1
# Some results seem to be printed even though
# they are incorrect: not sure why. However,
# 2nd guess solution I tried worked.
"""
gset = []
gen_ps(gset, [ 0, 0, 0, 0 ], 0)
for guess in gset:
    for code in cset:
        resp = check(guess, code)
        cpy = list(cset)
        cpy = reduce(cpy, code, resp)
        if len(cpy) == 1:
            print(cpy, resp)
"""
